class Point {
  public double x;
  public double y;
}

// Метод интерполяции Ньютона
public class Newton
{
  // points — массив заданных точек
  // x — точка, для которой ведётся поиск значения
  // возвращает интерполированное ~y(x)
  public static double interpolate(Point[] points, double x)
  {
    int n = points.length;
    double result = points[0].y;

    // Вычисление интерполяционного многочлена
    for (int i = 1; i < n; i++)
    {
      // Вычисление разделённых разностей
      double f = 0;
      for (int j = 0; j <= i; j++)
      {
        double d = 1;
        for (int k = 0; k <= i; k++)
        {
          if (k == j) continue;
          d *= (points[j].x - points[k].x);
        }
        f += points[j].y / d;
      }
      for (int k = 0; k < i; k++)
      {
        f *= (x - points[k].x);
      }
      result += f;
    }

    return result;
  }
}
