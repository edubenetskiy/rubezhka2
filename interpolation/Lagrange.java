class Point {
  public double x;
  public double y;
}

// Метод интерполяции Лагранжа
public class Lagrange
{
  // points — массив заданных точек
  // x — точка, для которой ведётся поиск значения
  // возвращает интерполированное ~y(x)
  public static double interpolate(Point[] points, double x)
  {
    int n = points.length;
    double result = 0;

    // Вычисление интерполяционного многочлена
    for (int i = 0; i < n; i++)
    {
      // Вычисление базисного полинома
      double l = 1;
      for (int j = 0; j < n; j++)
      {
        if (i == j) continue;
        double a = x - points[i].x;
        double b = points[i].x - points[j].x;
        l *= a / b;
      }
      result += points[i].y * l
    }

    return result;
  }
}
