public class Point {
  public double x;
  public double y;
}

/**
 * Этот класс реализует метод интерполяции кубическими сплайнами.
 *
 * Интерполянта рассчитывается однократно при инициализации,
 * после чего можно обращаться к модели за приближёнными значениями.
 */
public class CubicSpline
{
  // Узлы интерполяции
  private double[] points;

  // Число узлов интерполяции
  private int n;

  /*
   * Коэффициенты сплайнов.
   * Важно: свободные члены равны значениям в узлах интерполяции.
   */
  private double[] a; // свободные члены
  private double[] b; // первые производные
  private double[] c; // вторые производные
  private double[] d; // третьи производные

  /**
   * Конструктор рассчитывает все сплайны и создаёт интерполянту.
   */
  public Spline(Point[] points) {
    this.points = points;
    this.n = points.length;

    // Свободные члены равны значениям функции на интерполяционной сетке.
    a = new double[x.length];
    for (int i = 0; i < n; i++) {
      this.a[i] = points[i].y;
    }

    this.c = findSecondDerivatives();
    findFirstAndThirdDerivatives();
  }

  /**
   * Рассчитывает вторые производные сплайнов методом прогонки
   * для трёхдиагональной матрицы (методом Люэлина Томаса).
   */
  private double[] findSecondDerivatives()
  {
    double[] c = new double[n];

    double[] alpha = new double[n];
    double[] beta  = new double[n];

    alpha[0] = 0;
    beta[0] = 0;

    double currentSegment = 0;
    double nextSegment = 0;

    double A = 0;
    double B = 0;

    // Прогонка

    for (int i = 1; i < n - 1; i++)
    {
      currentSegment = points[i].x - points[i - 1].x;
      nextSegment    = points[i + 1].x - points[i].x;

      double currentHeight = points[i].y - points[i - 1].y;
      double nextHeight    = points[i + 1].y - points[i].y;

      A = 2 * (currentSegment + nextSegment);
      B = 6 * (nextHeight / nextSegment - currentHeight / currentSegment);

      double tmp = currentSegment * alpha[i - 1] + A;

      alpha[i] = -nextSegment / tmp;
      beta[i] = (B - currentSegment * beta[i - 1]) / tmp;
    }

    // Обратная подстановка

    c[n - 1] = (B - currentSegment * beta[n - 2])
             / (A + currentSegment * alpha[n - 2]);

    for (int i = n - 2; i > 0; i--) {
      c[i] = alpha[i] * c[i + 1] + beta[i];
    }

    return c;
  }

  /**
   * Рассчитывает первую и третью производные по известным значениям второй.
   */
  private void findFirstAndThirdDerivatives()
  {
    this.b = new double[n];
    this.d = new double[n];

    for (int i = n - 1; i > 0; i--)
    {
        double currentSegment = points[i].x - points[i - 1].x;
        double currentHeight  = points[i].y - points[i - 1].y;
        b[i] = currentSegment * (2 * c[i] + c[i - 1]) / 6
             + currentHeight / currentSegment;
        d[i] = (c[i] - c[i - 1]) / currentSegment;
    }
  }

  /**
   * Возвращает приближённое значение функции в заданной точке x.
   */
  public double interpolate(double x)
  {
    // ищем отрезок, содержащий точку x
    int i = 0;
    while (points[i + 1].x < x) i++;

    // длина отрезка
    double dx = x - points[i].x;

    // вычисляем значение на соответствующем сплайне
    double y = a[i]
             + b[i] * dx
             + c[i] * dx * dx
             + d[i] * dx * dx * dx;

    return y;
  }
}
