# Рубежный контроль № 2 по вычислительной математике

## Решение обыкновенных дифференциальных уравнений

Здесь приведены гиперссылки на решения, находящиеся
в каталоге `differential-equations`.

- [Метод Эйлера](differential-equations/Euler.java)
- [Усовершенствованный метод Эйлера](differential-equations/ImprovedEuler.java)
- [Метод Адамса](differential-equations/Adams.java)
- [Метод Милна](differential-equations/Milne.java)

## Интерполяция

Здесь приведены гиперссылки на решения, находящиеся
в каталоге `interpolation`.

- [Метод Ньютона](interpolation/Newton.java)
- [Метод Лагранжа](interpolation/Lagrange.java)
- [Метод кубических сплайнов](interpolation/CubicSpline.java)

-----

Сделал с :heart:  
Егор Дубенецкий