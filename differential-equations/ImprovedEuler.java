public class Point {
  public double x;
  public double y;
}

// Усовершенствованный метод Эйлера
public class ImprovedEuler {

  // Заданная функция f(x, y), где y = y(x)
  public static double f(double x, double y)
  {
      return Math.sin(2*x) + y*y; // функцию вписать сюда
  }

  // n — число интервалов
  // [x0, xn] — отрезок, на котором ищется решение
  // y0 — значение y(x0)
  // возвращает множество точек (x, y(x))
  public static Point[] solve(int n, double x0, double y0, double xn)
  {
      Point[] points = new Point[n];

      // Шаг разбиения отрезка [x0, xn] на интервалы
      double h = (xn - x0) / n;

      // Формирование системы равноотстоящих узлов
      for (int i = 0; i < n; i++)
      {
          points[i].x = x0 + i * h;
      }

      points[0].y = y0;

      // Вычисление значений функции в узловых точках
      for (int i = 1; i < n; i++)
      {
          Point previous = points[i - 1];

          // Вычисление значения в промежуточной точке
          Point intermediate = new Point();
          intermediate.x = previous.x + h/2;
          intermediate.y = previous.y + h/2 * f( previous.x, previous.y );

          points[i].y = previous.y + h * f( intermediate.x, intermediate.y );
      }

      return points;
  }
}
