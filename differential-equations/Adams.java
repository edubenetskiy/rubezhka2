public class Point {
  public double x;
  public double y;
}

// Метод Адамса
public class Adams {

  // Заданная функция f(x, y), где y = y(x)
  public static double f(double x, double y)
  {
      return Math.sin(2*x) + y*y; // функцию вписать сюда
  }

  public static double f(Point p)
  {
      return f(p.x, p.y);
  }

  // n — число интервалов
  // [x0, xn] — отрезок, на котором ищется решение
  // y0 — значение y(x0)
  // возвращает множество точек (x, y(x))
  public static Point[] solve(int n, double x0, double y0, double xn)
  {
      Point[] points = new Point[n];

      // Шаг разбиения отрезка [x0, xn] на интервалы
      double h = (xn - x0) / n;

      // Формирование системы равноотстоящих узлов
      for (int i = 0; i < n; i++)
      {
          points[i].x = a + i * h;
      }

      points[0].y = y0;

      // Вычисление значений в трёх узловых точках методом Рунге — Кутта
      for (int i = 1; i < 4; i++)
      {
          Point previous = points[i-1];

          // Расчёт коэффициентов
          double k1 = f(previous.x,        previous.y);
          double k2 = f(previous.x + h/2,  previous.y + h/2 * k1);
          double k3 = f(previous.x + h/2,  previous.y + h/2 * k2);
          double k4 = f(previous.x + h,    previous.y + h   * k3);

          // Расчёт приращения и очередного значения y = y(x)
          double delta = h/6 * (k1 + 2*k2 + 2*k3 + k4);
          points[i].y = previous.y + delta;
      }

      // Вычисление значений в остальных точках
      for (int i = 4; i < n; i++)
      {
          // Вычисление предиктора
          double predictor = points[i-4].y + h / 24 * (
                                -9 * f(points[i-4])
                               +37 * f(points[i-3])
                               -59 * f(points[i-2])
                               +55 * f(points[i-1])
                             );

          // Вычисление корректора
          points[i].y      = points[i-1].y + h / 24 * (
                                     f(points[i-3])
                                -5 * f(points[i-2])
                               +19 * f(points[i-1])
                                +9 * f(points[i].x, predictor)
                             );
      }

      return points;
  }
}
